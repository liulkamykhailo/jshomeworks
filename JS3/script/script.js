//1.Цикли дозволяють виконувати один і той самий шматок коду по суті до нескінченності. Повторюють дії доки цикл не завершиться.
//2. Цикл for використовуємо коли потрібно виконати певну кількість разів
// while буде виконуватись доки умова циклу є істинною
// do-while виконає цикл мінімум 1 раз навіть якщо умова false
//3. Явне перетворення типів відбувається за допомогою спеціальних команд розробника. Неявне перетворення відбувається автоматично.

let number = +prompt('Задайте ваше число кратне 5');
while (isNaN(number) || number === null || !number || number === undefined || number === "") {
    number = +prompt('Введіть число кратне 5');
};
console.log(number);


// // for (let i = 0; i <= number; i++) {
// //     if (i % 5 === 0 || i === 0) {
// //         continue;
// //         console.log("Sory no numbers");
// //     } else {
// //         console.log(i);
// //     }
// // };


// for (let i = 0; i <= number; i++) {
//     if (i % 5 === 0 || i === 0) {
//         console.log(i);
//     }
// };
 
// // if (number === 0 || number % 5 !== 0 || number < 4) {
// //     console.log("Sory no numbers");
// // };





if (number < 5) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 1; i <= number; i++) {
        if (i % 5 === 0 || i === 0) {
            console.log(i);
        }
    }
}

