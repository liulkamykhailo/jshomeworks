//1.Екранування потрібне аби синтаксичний символ мови програмування читався як звичайний. Наприклад апостроф в строчці, або /
//2.Function declarstion - оголошується словом function, Function expression - записується в змінну і не має ключового слова
//3.Підняття вгору, наприклад функцію можна оголосити до оголошення змінної і вона буде працювати


const newUser = {};
function createNewUser() {
    newUser.firstName = prompt("Введіть ваше ім'я");
    newUser.lastName = prompt("Введіть ваше прізвище");
    newUser.dateUser = prompt("Введіть дату вашого народження у форматі: dd.mm.yyyy ");

    newUser.getAge = function getAge() {
        let dateBirthday = new Date((this.dateUser.slice(6)) + "." + (this.dateUser.slice(3, 5)) + "." + (this.dateUser.slice(0, 2))).getTime();
        let today = new Date().getTime();
        return Math.floor((today - dateBirthday)/(60*60*1000*24*365.25))
    };
    
    newUser.getPassword = function getPassword() {
        let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.dateUser.slice(6);
        return password;
    };

    newUser.getLogin = function getLogin() {
        let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return login;
    }
    return newUser;
};

createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

