//1. За допомогою let або const. Зустрічається також застаріле var(його бажано не використовувати)
//2. Confirm - викликає вікно з 2 кнопками так чи ні та повертає true or false, Prompt - викликає вікно для вводу даних користувача
//3. Неявне перетворення типів відбувається автоматично. При застосуванні операторів до різних типів. Пр.: alert('4'/'2'); //2        alert(4+'2'); //42




//1
let myName = 'Misha';
let admin = myName;
console.log(admin);
//2
let days = Math.floor(Math.random()*10)+1;
console.log(days);
let daysInSeconds = days * 86400
console.log(daysInSeconds);
//3
let userName = prompt("What is your name?", '');
console.log(userName);

let userAge = prompt("What is your age", "");
console.log(+userAge);