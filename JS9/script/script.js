// 1. За допомогою document.createElement, або в самому HTML файлі
// 2. Параметр куди вставляти елемент, є 4 значення beforebegin, afterbegin, beforeend, afterend
// 3. Для видалення вузла є метод node.remove()


let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function list(a, b) {
    let ul = document.createElement('ul');

    a.map(elem => {
        let li = document.createElement('li');
        li.textContent = elem;
        ul.appendChild(li);
        b.append(ul);
    })
};

list(arr, document.body);