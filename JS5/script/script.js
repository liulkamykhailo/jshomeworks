//1.Методи - це вбудовані функції всередині об'єкта, які працюють з змінними самого об'єкта
//2.Об'єкт може мати будь який тип даних
//3.Об'єкти копіюються за посиланням, а от наприклад примітивні типи даних такі як рядки, числа копіюються за значенням. Сам об'єкт не дублюється через =, а копіюється посилання.



const newUser = {};

function createNewUser() {
    newUser.firstName = prompt("Введіть ваше ім'я");
    newUser.lastName = prompt("Введіть ваше прізвище");
    newUser.getLogin = function getLogin() {
        let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        return login;
    }
    return newUser;
};

createNewUser();
console.log(newUser);
console.log(newUser.getLogin());