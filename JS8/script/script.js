//1 DOM - це по суті весь HTML документ і все що в ньому є пробіли, коментарі, теги
//2 innerText вивовдить тільки текст,  innerHTML повертає текст разом з тегами
//3 За допомогою методів getElementBy* та querySelector*


//1
let p = document.getElementsByTagName("p");
for (const paragraph of p) {
    paragraph.style.backgroundColor = "#ff0000";
};

//2
let optionsList = document.getElementById("optionsList");
console.log(optionsList);
// let optionsListParent = document.getElementById("optionsList").parentNode;
let optionsListParent = optionsList.parentNode;
console.log(optionsListParent);

let optionListChilds = optionsList.childNodes;
for (const child of optionListChilds) {
    // console.log(child.nodeName);
    // console.log(child.nodeType);
    console.log(`Name: ${child.nodeName}  Type: ${child.nodeType}`);
}

//3
let testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

//4
let mainHeader = document.querySelector('.main-header');
let mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren)
for (let headerChild of mainHeaderChildren ) {
    headerChild.classList.add('nav-item');
}
// Клас nav-item потрібно було додати для всіх дочірніх елементів (навіть вкладених)

//5
let sectionTitle = document.querySelectorAll(".section-title");
for (const section of sectionTitle) {
    section.classList.remove('section-title');
}