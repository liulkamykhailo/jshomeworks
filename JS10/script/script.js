// let button = document.querySelector('button');
// let div = document.querySelector('#text')
// function outDiv() {
//     div.style.display = 'none';
// }
// button.addEventListener('click', outDiv);


let tab = document.querySelectorAll(".tabs-title");
let content = document.querySelectorAll(".content");

for (let i = 0; i < tab.length; i++){
    tab[i].addEventListener("click", (event) => {
        let tabChild = event.target.parentElement.children;
        for (let j = 0; j < tabChild.length; j++) {
            tabChild[j].classList.remove("active");
        }
        tab[i].classList.add("active");
        
        let tabContent = event.target.parentElement.nextElementSibling.children;
        for (let k = 0; k < tabContent.length; k++) {
            tabContent[k].classList.remove("content-active");
        }
        content[i].classList.add("content-active");
    });
}